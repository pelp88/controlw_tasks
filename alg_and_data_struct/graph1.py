import time
import collections
import random
import matplotlib.pyplot as plt


def with_qs(x):
    """
    Counting quantities of numbers in given list

    :param x: list of numbers
    :return <dict>: keys - numbers, values - quantities of numbers
    """

    def quicksort(nums):
        if len(nums) <= 1:
            return nums
        else:
            q = random.choice(nums)
        l_nums = [n for n in nums if n < q]
        e_nums = [q] * nums.count(q)
        b_nums = [n for n in nums if n > q]
        return quicksort(l_nums) + e_nums + quicksort(b_nums)

    x = quicksort(x)
    x_out = {}
    count = 0
    for i in range(len(x)):
        if len(x) > 1:
            if x[0] == x[1]:
                count += 1
                x.pop(0)
            else:
                count += 1
                x_out[x[0]] = count
                count = 0
                x.pop(0)

    return x_out


data_int = [random.randrange(0, 10, 1) for _ in range(1000)]
data_float = [random.randrange(0, int(10 / 0.5)) * 0.5 for _ in range(1000)]


if int(input('Choose count method (1 - with collections.Counter; 2 - with quicksort algorithm): ')) == 1:
    start_time = time.perf_counter()
    output1 = collections.Counter(data_int)
    print('Int = ', '{:f}'.format(float(time.perf_counter() - start_time)))
    output2 = collections.Counter(data_float)
    print('Float = ', '{:f}'.format(float(time.perf_counter() - start_time)))
else:
    start_time = time.perf_counter()
    output1 = with_qs(data_int)
    print('Int = ', '{:f}'.format(float(time.perf_counter() - start_time)))
    output2 = with_qs(data_float)
    print('Float = ', '{:f}'.format(float(time.perf_counter() - start_time)))


fig, ax = plt.subplots()
fig, bx = plt.subplots()

ax.bar(*zip(*sorted(output1.items())))
bx.bar(*zip(*sorted(output2.items())))

ax.set_autoscale_on(True)
bx.set_autoscale_on(True)

ax.set_title("Integer")
bx.set_title("Float")

plt.show()
