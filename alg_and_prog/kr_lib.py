def saxpy(x: list, y: list, a) -> list:
    """
    Vector addition

    :param x: vector x
    :param y: vector y
    :param a: scalar a
    :return <list>: resulting vector
    """
    return [a * x_item + y_item for x_item, y_item in zip(x, y)]


def matrix_multi(a, b):
    """
    Matrix multiplication

    :param a: matrix A
    :param b: matrix B
    :return <list of lists>: resulting matrix
    """
    zip_b = list(zip(*b))
    return [[sum(ele_a * ele_b for ele_a, ele_b in zip(row_a, col_b)) for col_b in zip_b] for row_a in a]


def gauss_jordan(coeffs_matrix, answers_matrix):
    """
    Gauss-Jordan elimination algorithm

    :param coeffs_matrix: matrix of coefficients
    :param answers_matrix: matrix of answers
    :return <list>: list of all x's
    """

    def row_divider(mat1, mat2, row, divider):
        mat1[row] = [a / divider for a in mat1[row]]
        mat2[row] /= divider

    def row_comb(mat1, mat2, row, source_row, weight):
        mat1[row] = [(a + k * weight) for a, k in zip(mat1[row], mat1[source_row])]
        mat2[row] += mat2[source_row] * weight

    col = 0
    while col < len(answers_matrix):
        current_row = None
        for r in range(col, len(coeffs_matrix)):
            if current_row is None or abs(coeffs_matrix[r][col]) > abs(coeffs_matrix[current_row][col]):
                current_row = r

        if current_row is None:
            return None

        if current_row != col:
            coeffs_matrix[current_row], coeffs_matrix[col] = coeffs_matrix[col], coeffs_matrix[current_row]
            answers_matrix[current_row], answers_matrix[col] = answers_matrix[col], answers_matrix[current_row]

        row_divider(coeffs_matrix, answers_matrix, col, coeffs_matrix[col][col])

        for r in range(col + 1, len(coeffs_matrix)):
            row_comb(coeffs_matrix, answers_matrix, r, col, -coeffs_matrix[r][col])
        col += 1

    output = [0 for b in answers_matrix]
    for i in range(len(answers_matrix) - 1, -1, -1):
        output[i] = answers_matrix[i] - sum(x * a for x, a in zip(output[(i + 1):], coeffs_matrix[i][(i + 1):]))

    return output


def cramer(coeffs_matrix, answers_matrix):
    """
    Cramer's rule algorithm

    :param coeffs_matrix: matrix of coefficients
    :param answers_matrix: matrix of answers
    :return <list>: list of all x's
    """

    def matrix_minor(matrix, low, high):
        return [row[:high] + row[high + 1:] for row in (matrix[:low] + matrix[low + 1:])]

    def matrix_det(matrix):
        if len(matrix) == 2:
            return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0]
        else:
            return sum(((-1) ** i) * matrix[0][i] * matrix_det(matrix_minor(matrix, 0, i)) for i in range(len(matrix)))

    output = []
    d = matrix_det(coeffs_matrix)
    for i in range(len(coeffs_matrix)):
        output.append(matrix_det([coeffs_matrix[i] if j != i else answers_matrix for j in range(len(coeffs_matrix))]) / d)

    return output

