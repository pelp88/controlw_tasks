import time
import kr_lib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import random
import sys

n_min = 10
n_max = 100
n_step = 10

counter = 6
size_data = []
time_data = []
n_data = []


for n in range(int(n_min), int(n_max), int(n_step)):
    tmp_mat1 = [[random.uniform(-10, 10) for j in range(n)] for i in range(n)]
    tmp_mat2 = [random.uniform(-10, 10) for i in range(n)]
    counter += 1
    averages_time = []
    averages_size = []
    for _ in range(3):
        start = time.time()
        averages_size.append(sys.getsizeof(kr_lib.gauss_jordan(tmp_mat1, tmp_mat2)))
        averages_time.append(time.time() - start)
    time_data.append(sum(averages_time) / 3)
    size_data.append(sum(averages_size) / 3)
    print(time_data[-1])
    n_data.append(n)


fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
plt.plot(n_data, time_data, size_data, 'o')
trend1 = np.poly1d(np.polyfit(n_data, time_data, 1))
trend2 = np.poly1d(np.polyfit(n_data, size_data, 1))
plt.plot(n_data, trend1(n_data), trend2(n_data), 'b', c='red')
plt.xlabel('длина матриц')
plt.ylabel('время выполнения')

plt.show()
