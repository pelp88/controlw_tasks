import kr_lib

choice = input('Float or int?: ')
if 'float' or 'Float' or 'FLOAT' in choice:
    x_vector = list(map(float, input('Enter X vector: ').split()))
    y_vector = list(map(float, input('Enter Y vector: ').split()))
    a_scalar = float(input('Enter A: '))
elif 'int' or 'Int' or 'INT' in choice:
    x_vector = list(map(int, input('Enter X vector: ').split()))
    y_vector = list(map(int, input('Enter Y vector: ').split()))
    a_scalar = int(input('Enter A: '))

print(f'Result is {", ".join(kr_lib.saxpy(x_vector, y_vector, a_scalar))}')
