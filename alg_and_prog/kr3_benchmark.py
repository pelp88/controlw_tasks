import time
import kr_lib
import numpy as np
import matplotlib.pyplot as plt
import random

a_float = 10.5
a_int = int(a_float)
n_min = 10e5
n_max = 10e6
n_step = 10e5


n_int_data = []
n_float_data = []
time_int_data = []
time_float_data = []

for n in range(int(n_min), int(n_max), int(n_step)):
    averages = []
    temp = [random.uniform(-10, 10) for i in range(n)]
    for _ in range(3):
        start = time.time()
        kr_lib.saxpy(temp, temp, a_float)
        averages.append(time.time() - start)
    average = sum(averages) / 3
    print(average)
    n_float_data.append(n)
    time_float_data.append(average)

for n in range(int(n_min), int(n_max), int(n_step)):
    averages = []
    temp = [random.randint(-10, 10) for i in range(n)]
    for _ in range(3):
        start = time.time()
        kr_lib.saxpy(temp, temp, a_int)
        averages.append(time.time() - start)
    average = sum(averages) / 3
    print(average)
    n_int_data.append(n)
    time_int_data.append(average)


plt.plot(n_float_data, time_float_data, 'o')
trend_float = np.poly1d(np.polyfit(n_float_data, time_float_data, 1))
plt.plot(n_float_data, trend_float(n_float_data), 'b', c='blue', label=f'Функция тренда float - {trend_float}')

plt.plot(n_int_data, time_int_data, 'o')
trend_int = np.poly1d(np.polyfit(n_int_data, time_int_data, 1))
plt.plot(n_int_data, trend_int(n_int_data), 'b', c='red', label=f'Функция тренда int - {trend_int}')

plt.legend()
plt.xlabel('длина векторов')
plt.ylabel('время выполнения, с.')

plt.show()
