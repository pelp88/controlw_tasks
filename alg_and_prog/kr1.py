print("Sum: ", 42 + sum([1 / int(line.rstrip()) for line in __import__('sys').stdin if
                         all(char.isdigit() for char in line.rstrip()) and line.rstrip() != '']))
