import time  # Importing some useful stuff
import os


def output_string(index, input_lst):
    a = []
    for i in range(index, index + 23):
        a.append("\033[36m" + input_lst[i])
    return "".join(a)


os.system('')
lines = []  # Opening text file with frames and initializing main vars
for line in __import__('sys').stdin:
    lines.append(line)
gif = []

print("If you want to exit program press Ctrl-C")
time.sleep(3)

print("\033c", end="")  # Making console nice and clean :3

try:
    count = 0
    while True:
        if lines[count].count("```") == 1:
            gif.append(output_string(count + 1, lines))  # Parsing text file and collecting frames
            count += 25
        elif lines[count].count("```") != 1:
            count += 1
except IndexError:
    pass

try:
    while True:
        for frame in gif:
            time.sleep(0.5)  # Animation drawing
            print(frame)
            print("\033c", end="")
except KeyboardInterrupt:
    print("Stopped by user's command")
