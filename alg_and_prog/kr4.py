import time
import kr_lib
import matplotlib.pyplot as plt
import numpy as np
import random

n_min = 10e3
n_max = 10e5
n_step = 10e4

counter = 6
time_data = []
n_data = []


for n in range(int(n_min), int(n_max), int(n_step)):
    tmp_mat1 = [[random.uniform(-10, 10) for j in range(counter)] for i in range(n)]
    tmp_mat2 = [[random.uniform(-10, 10) for j in range(counter)] for i in range(n)]
    counter += 1
    averages = []
    for _ in range(3):
        start = time.time()
        temp1 = kr_lib.matrix_multi(tmp_mat1, tmp_mat2)
        averages.append(time.time() - start)
    time_data.append(sum(averages) / 3)
    print(time_data[-1])
    n_data.append(n)


plt.plot(n_data, time_data, 'o')
trend = np.poly1d(np.polyfit(n_data, time_data, 1))
plt.plot(n_data, trend(n_data), 'b', c='red', label=f'Функция тренда - {trend}')
plt.legend()
plt.xlabel('длина матриц')
plt.ylabel('время выполнения, с.')

plt.show()
